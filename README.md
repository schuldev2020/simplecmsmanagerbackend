# SimpleCmsManagerBackend

A reactive Spring Boot Rest Api.

**Getting Started**

# Requirements

*  Java 11
*  Internet Connection to download Maven Dependencies

**Install Java 11**

https://www.oracle.com/java/technologies/javase-jdk11-downloads.html

**Install Maven 3.6**

https://maven.apache.org/download.cgi

If you don't want or can't install Maven. Use the Maven Wrapper inside the root folder. 

**Check Java and Maven**

Java should be 11.x.x:

`java -version`

Maven should be 3.6.x:
If you use the contained Maven Wrapper:

`mvnw -v`

# Using Spring Boot Maven Plugin to start Application

**Start Application with Maven Wrapper**

`mvnw spring-boot:run`

**Start Application with installed Maven**

`mvn spring-boot:run`

# Using Java to start Application

1.   Build Application

     Open Console in Root dir of the Project:

     `mvn install`

     without tests:

     `mvn install -DskipTests`


2.  Start Application

     `java -jar target/cms-x.x.jar`

# Using Docker to start Application

Download maven_java11 Image from Registry //TODO: upload maven_java11 to registry!!!!!!!!!!

Or create your own Docker Image with java 11 and maven 3.6

Build docker Image with Application

`docker build -t simpleCmsManagerBackend .`

Run docker Image with Application

```
docker run -d \
--name simpleCmsManagerBackend \
-p 8080:8080 \
simpleCmsManagerBackend
```



# Usage

Start Application

Curl or Visit

`localhost:8080`

