package com.cms.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.cms.model.item.*;
import com.cms.service.CmsComponentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class InitializeDatabase implements InitializingBean {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    private CmsComponentService cmsComponentService;



    @Override
    public void afterPropertiesSet() {
        cmsComponentService.getAllCmsComponent().hasElements().subscribe(hasElements -> {
            if(!hasElements) {
                for (int i = 0; i < 2; i++) {
                    CmsComponent cmsComponent = new CmsComponent();
                    cmsComponent.setName("testname" + i);
                    cmsComponent.setDescription("testdescription" + i);
                    List<CmsField> cmsFieldList = new ArrayList<>();
                    for (int k = 0; k < 2; k++) {
                        CmsField cmsField = new CmsField();
                        cmsField.setId((long) k);
                        cmsField.setName("test field name" + k);
                        cmsField.setDescription("test field description" + k);
                        cmsField.setText("test field text" + k);
                        cmsFieldList.add(cmsField);
                    }
                    cmsComponent.setCmsFieldList(cmsFieldList);
                    cmsComponentService.save(Mono.just(cmsComponent)).subscribe(result -> log.info("Entity has been saved: {}", result));
                }
            }
        });
    }
}
