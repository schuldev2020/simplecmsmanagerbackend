package com.cms.service;

import com.cms.model.item.CmsComponent;
import reactor.core.publisher.Flux;

public interface CmsComponentService extends RepositoryService<CmsComponent> {

    Flux<CmsComponent> getAllCmsComponent();
}
