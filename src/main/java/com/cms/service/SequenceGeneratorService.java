package com.cms.service;

public interface SequenceGeneratorService {

   long generateSequence(String seqName);
}
