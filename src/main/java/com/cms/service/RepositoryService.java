package com.cms.service;

import reactor.core.publisher.Mono;

public interface RepositoryService<T extends Object> {

    Mono<T> save(Mono<T> object);

    Mono<T> getByID(Long id);

    Mono<Void> deleteById(Long id);
}
