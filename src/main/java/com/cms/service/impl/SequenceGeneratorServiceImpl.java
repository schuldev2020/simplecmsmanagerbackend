package com.cms.service.impl;

import java.util.Objects;

import javax.annotation.Resource;

import com.cms.model.DatabaseSequence;
import com.cms.service.SequenceGeneratorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class SequenceGeneratorServiceImpl implements SequenceGeneratorService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Resource
    MongoOperations mongoOperations;

    public long generateSequence(String seqName) {
        DatabaseSequence counter = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq", 1), options().returnNew(true).upsert(true),
                DatabaseSequence.class);
        log.info(seqName + " saved with Id " + (!Objects.isNull(counter) ? counter.getSeq() : 0));
        return !Objects.isNull(counter) ? counter.getSeq() : 0;
    }
}
