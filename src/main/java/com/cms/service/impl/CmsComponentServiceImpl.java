package com.cms.service.impl;

import com.cms.model.item.CmsComponent;
import com.cms.repository.CmsComponentRepository;
import com.cms.service.CmsComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CmsComponentServiceImpl implements CmsComponentService {

    private final CmsComponentRepository cmsComponentRepository;

    @Autowired
    public CmsComponentServiceImpl(final  CmsComponentRepository cmsComponentRepository) {
        this.cmsComponentRepository = cmsComponentRepository;
    }

    @Override
    public Mono<CmsComponent> save(final Mono<CmsComponent> object) {
        return object.flatMap(cmsComponentRepository::save);
    }

    @Override
    public Mono<CmsComponent> getByID(final Long id) {
        return cmsComponentRepository.findById(id);
    }

    @Override
    public Flux<CmsComponent> getAllCmsComponent() {
        return cmsComponentRepository.findAll();
    }

    @Override
    public Mono<Void> deleteById(final Long id) {
        return cmsComponentRepository.deleteById(id);
    }
}
