package com.cms.controller;


import com.cms.model.item.CmsComponent;
import com.cms.service.CmsComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
public class CmcComponentController {

    private final CmsComponentService cmsComponentService;

    @Autowired
    public CmcComponentController(final CmsComponentService cmsComponentService) {
        this.cmsComponentService = cmsComponentService;
    }

    @GetMapping("/components")
    public Flux<CmsComponent> getAllComponent() {
        return cmsComponentService.getAllCmsComponent();
    }

    @GetMapping("/component/{id}")
    public Mono<CmsComponent> getComponentById(@PathVariable long id) {
        return cmsComponentService.getByID(id);
    }

    @RequestMapping(value = "/components", method = RequestMethod.PUT)
    public Mono<CmsComponent> createCmsComponent(@RequestBody Mono<CmsComponent> cmsComponent) {
        return cmsComponentService.save(cmsComponent);
    }

    @RequestMapping(value = "/component/{id}", method = RequestMethod.DELETE)
    public Mono<Void> deleteCmsComponent(@PathVariable long id) {
        return cmsComponentService.deleteById(id);
    }

    @RequestMapping(value = "/component/{id}", method = RequestMethod.PATCH)
    public Mono<CmsComponent> updateCmsComponent(@PathVariable long id, @RequestBody Mono<CmsComponent> cmsComponent) {
        return cmsComponentService.save(cmsComponent.map(component -> {
            component.setId(component.getId());
            return component;
        }));
    }
}
