package com.cms.repository.listener;

import javax.annotation.Resource;

import com.cms.model.item.CmsComponent;
import com.cms.service.SequenceGeneratorService;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;


@Component
public class CmsComponentListener extends AbstractMongoEventListener<CmsComponent> {

    @Resource
    SequenceGeneratorService sequenceGeneratorService;

    @Override
    public void onBeforeConvert(final BeforeConvertEvent<CmsComponent> event) {
        if (event.getSource().getId() == null || event.getSource().getId() <= 0) {
            event.getSource().setId(sequenceGeneratorService.generateSequence(CmsComponent.class.getSimpleName()));
        }
    }
}
