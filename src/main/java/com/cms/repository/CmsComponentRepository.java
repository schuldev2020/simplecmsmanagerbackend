package com.cms.repository;

import com.cms.model.item.CmsComponent;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface CmsComponentRepository extends ReactiveMongoRepository<CmsComponent, Long> {


}
