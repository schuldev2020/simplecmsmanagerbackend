package com.cms.model;

import java.util.List;

public abstract class AbstractCmsTemplate extends CmsObject {

    List<String> cmsStringList;

    public AbstractCmsTemplate(final List<String> cmsStringList) {
        super();
        this.cmsStringList = cmsStringList;
    }

    public AbstractCmsTemplate() {
        super();
    }

    public List<String> getCmsStringList() {
        return cmsStringList;
    }

    public void setCmsStringList(final List<String> cmsStringList) {
        this.cmsStringList = cmsStringList;
    }
}
