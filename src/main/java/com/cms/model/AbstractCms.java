package com.cms.model;

import java.util.Date;
import java.util.List;

import com.cms.model.item.CmsField;

public abstract class AbstractCms extends CmsObject {

    private List<CmsField> cmsFieldList;


    public AbstractCms(final List<CmsField> cmsFieldList) {
        super();
        this.cmsFieldList = cmsFieldList;
    }

    public AbstractCms() {
        super();
    }

    public List<CmsField> getCmsFieldList() {
        return cmsFieldList;
    }

    public void setCmsFieldList(final List<CmsField> cmsFieldList) {
        super.setLastModified(new Date());
        this.cmsFieldList = cmsFieldList;
    }
}
