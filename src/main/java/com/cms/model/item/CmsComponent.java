package com.cms.model.item;

import java.util.Date;
import java.util.List;

import com.cms.model.AbstractCms;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CmsComponent extends AbstractCms {

    private List<CmsComponentItem> cmsComponentItem;

    public CmsComponent( final List<CmsField> cmsFieldList, final List<CmsComponentItem> cmsComponentItem) {
        super(cmsFieldList);
        this.cmsComponentItem = cmsComponentItem;
    }

    public CmsComponent(final List<CmsComponentItem> cmsComponentItem) {
        super();
        this.cmsComponentItem = cmsComponentItem;
    }

    public CmsComponent() {
        super();
    }

    public List<CmsComponentItem> getCmsItemList() {
        return cmsComponentItem;
    }

    public void setCmsItemList(final List<CmsComponentItem> cmsComponentItem) {
        super.setLastModified(new Date());
        this.cmsComponentItem = cmsComponentItem;
    }
}
