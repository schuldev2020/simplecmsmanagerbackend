package com.cms.model.item;

import java.util.Date;

import com.cms.model.CmsObject;
import org.springframework.data.mongodb.core.mapping.Document;

public class CmsField extends CmsObject {
    String text;

    public CmsField( final String text) {
        super();
        this.text = text;
    }

    public CmsField() {
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        super.setLastModified(new Date());
        this.text = text;
    }
}
