package com.cms.model.item;

import java.util.List;

import com.cms.model.AbstractCms;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CmsComponentItem extends AbstractCms {

    public CmsComponentItem( final List<CmsField> cmsFieldList) {
        super(cmsFieldList);
    }

    public CmsComponentItem() {
        super();
    }
}
