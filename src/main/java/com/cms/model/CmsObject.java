package com.cms.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

public abstract class CmsObject {
    @Id
    private Long id;
    private Date creationDate;
    private Date lastModified;
    private String name;
    private String description;

    public CmsObject() {
        this.creationDate = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    protected void setLastModified(final Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        setLastModified(new Date());
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        setLastModified(new Date());
        this.description = description;
    }
}
